
public class FivePatterns2 {

	public static void main(String[] args) {

		// patternSize can be any integer of type int.
		// Just be careful what you wish for.
		int patternSize = 7;

		// Make sure it is always positive.
		patternSize = Math.abs(patternSize);

		// Zero in, zero out.
		if (patternSize == 0) {
			System.out.println("0");
		}

		// We have 5 patterns.
		for (int pattern = 1; pattern <= 5; pattern++) {

			System.out.println(); // New line before each pattern.

			for (int row = 1; row <= patternSize; row++) {

				// First and last rows for every pattern are the same.
				if ((row == 1) || (row == patternSize)) {

					for (int column = 1; column <= patternSize; column++) {
						System.out.print("# ");
					}

				} else {

					switch (pattern) { // Different case for every pattern.

					case 1: { // Pattern (a).

						for (int column = 1; column <= patternSize; column++) {

							if ((column == 1) || (column == patternSize)) {
								System.out.print("# ");
							} else {
								System.out.print("  ");
							}
						}

						break;
					}

					case 2: { // Pattern (b).

						for (int column = 1; column <= (row - 1); column++) {
							System.out.print("  ");
						}

						System.out.print("# ");

						break;
					}

					case 3: { // Pattern (c).

						for (int column = 1; column <= (patternSize
								- row); column++) {
							System.out.print("  ");
						}

						System.out.print("# ");

						break;
					}

					case 4: { // Pattern (d).

						/*
						 * Thanks to Andrejus Novatorovas (@ChronoD) for
						 * _simple_ solution to patterns (d) and (e).
						 */

						for (int column = 1; column <= patternSize; column++) {

							if (column == row) {
								System.out.print("# ");

								// No need for additional spaces past some
								// point.
								if (column >= ((patternSize - row) + 1)) {
									break;
								}

							} else if (column == ((patternSize - row) + 1)) {
								System.out.print("# ");

								// No need for additional spaces past some
								// point.
								if (column > row) {
									break;
								}

							} else {
								System.out.print("  ");
							}

						}

						break;
					}

					case 5: { // Pattern (e).

						for (int column = 1; column <= patternSize; column++) {

							if ((column == 1) || (column == row)
									|| (column == ((patternSize - row) + 1))
									|| (column == patternSize)) {
								System.out.print("# ");
							} else {
								System.out.print("  ");
							}
						}

					}
					}

				}

				System.out.println(); // New line after each row.

			}

		}

	}

}
