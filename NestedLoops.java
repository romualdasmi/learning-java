import java.util.Scanner;

public class NestedLoops {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		System.out.print("Enter the size of the pattern: ");
		int patternSize = reader.nextInt();
		reader.close();

		patternSize = Math.abs(patternSize);

		System.out.println();
		firstPattern(patternSize);

		System.out.println();
		secondPattern(patternSize);

		System.out.println();
		thirdPattern(patternSize);

		System.out.println();
		fourthPattern(patternSize);

		System.out.println();
		fifthPattern(patternSize);
	}

	private static void firstPattern(int patternSize) {
		
		for (int row = 1; row <= patternSize; row++) {

			for (int column = 1; column <= (patternSize - row); column++) {
				System.out.print("  ");
			}

			for (int column = 1; column <= ((row * 2) - 1); column++) {
				System.out.print("# ");
			}

			System.out.println();
		}
	}

	private static void fifthPattern(int patternSize) {

		int maxValue;
		maxValue = (int) Math.pow(2, patternSize) / 2;

		for (int row = 1; row <= patternSize; row++) {

			for (int column = 1; column <= (patternSize - row); column++) {
				printEmptyPadding(maxValue);
			}

			for (int column = 1; column <= row; column++) {

				boolean isFirst = (column == 1);
				int nowValue = (int) Math.pow(2, (column - 1));

				printPaddedValue(maxValue, nowValue, isFirst);
			}

			for (int column = 1; column <= (row - 1); column++) {

				int nowValue = (int) Math.pow(2, (row - column - 1));
				boolean isFirst = false;

				printPaddedValue(maxValue, nowValue, isFirst);
			}

			System.out.println();
		}
	}

	private static void fourthPattern(int patternSize) {
		
		int maxValue;
		maxValue = patternSize;

		for (int row = 1; row <= patternSize; row++) {

			for (int column = 1; column <= row; column++) {

				printPaddedValue(maxValue, column, (column == 1));
			}

			for (int column = 1; column <= (((patternSize - row) * 2)
					- 1); column++) {

				printEmptyPadding(maxValue);
			}

			for (int column = 1; column <= row; column++) {

				if (row == patternSize) {

					printPaddedValue(maxValue, (row - column), false);

					if (column == (patternSize - 1))
						break;

				} else {

					printPaddedValue(maxValue, ((row - column) + 1), false);
				}

			}

			System.out.println();
		}
	}

	private static void thirdPattern(int patternSize) {
		
		int maxValue;
		maxValue = patternSize;

		for (int row = 1; row <= patternSize; row++) {

			for (int column = 1; column <= (patternSize - row); column++) {

				printEmptyPadding(maxValue);
			}

			for (int column = 1; column <= row; column++) {

				printPaddedValue(maxValue, column, (column == 1));
			}

			for (int column = 1; column <= (row - 1); column++) {

				printPaddedValue(maxValue, (row - column), false);
			}

			System.out.println();
		}
	}

	private static void secondPattern(int patternSize) {
		
		int maxValue = patternSize;

		for (int row = 1; row <= patternSize; row++) {

			for (int column = 1; column <= (row - 1); column++) {

				printEmptyPadding(maxValue);
			}

			for (int column = 1; column <= ((patternSize - row)
					+ 1); column++) {

				printPaddedValue(maxValue, column, (column == 1));
			}

			System.out.println();
		}
	}

	private static void printEmptyPadding(int maxValue) {

		int maxLength = String.valueOf(maxValue).length();
		String emptyPadding = " ";

		for (int i = 1; i <= maxLength; i++) {
			emptyPadding += " ";
		}

		System.out.print(emptyPadding);
	}

	private static void printPaddedValue(int maxValue, int nowValue,
			boolean isFirst) {

		int maxLength = String.valueOf(maxValue).length();
		int nowLenght = String.valueOf(nowValue).length();
		String nowPadding = " ";

		for (int i = 1; i <= (maxLength - nowLenght); i++) {
			nowPadding += " ";
		}

		if (isFirst) {
			System.out.print(nowValue);
		} else {
			System.out.print(nowPadding + nowValue);
		}

	}

}
